package netis

import com.twitter.finagle.Service
import com.twitter.finagle.http.Http
import com.twitter.util.Future
import org.jboss.netty.handler.codec.http.{
  DefaultHttpResponse,
  HttpVersion,
  HttpResponseStatus,
  HttpRequest,
  HttpResponse
}
import java.net.{ SocketAddress, InetSocketAddress }
import com.twitter.finagle.builder.{ Server, ServerBuilder }
import com.twitter.finagle.builder.ServerBuilder
import org.jboss.netty.buffer.ChannelBuffers._
import java.nio.ByteBuffer
import java.nio.charset.CharsetEncoder
import java.nio.charset.Charset
import java.nio.CharBuffer
import scala.collection.mutable.Map
import org.jboss.netty.buffer.ChannelBuffer

object NetisServer {

  private lazy val evalService =
    (routehandler: RouteHandler) => new Service[HttpRequest, HttpResponse] {
      def apply(req: HttpRequest) = {
        println(req.getUri())
        val request = NetisServerCommons.obtainRequest(req.getUri())
        val params = NetisServerCommons.obtainRequestParam(req.getUri())
        println("handling request .... ")
//        val r = request match {
//          case "/" => new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK)
//        }
        val r = routehandler.hanldeRoute(request)
        Future.value(r)
      }
    }

  def start(server: ServerBootStrap) {
    ServerBuilder().codec(Http()).bindTo(
      new InetSocketAddress(server.port)).name(server.serverName).build(evalService(server.routeHandler))
  }
}

case class ServerBootStrap(port: Int, serverName: String, routeHandler: RouteHandler)

object NetisServerCommons {
  val obtainRequest = (url: String) => url.split("/")(0)
  val obtainRequestParam = (url: String) => {
    val buffer = Map[String, String]()
    if (url.split("/").length > 1) {
      url.split("/")(1).split("&").foreach(str => {
        buffer.+=((str.split("=")(0), str.split("=")(1)))
      })
    }
    buffer
  }
}

abstract class RouteHandler {
  private var _params = Map[String, String]()
  def params = _params
  def params_=(value: Map[String, String]): Unit = _params = (value)
  def hanldeRoute(url: String) = resolveRoute(url)
  def contentResp(content: String) = wrappedBuffer(content.getBytes())
  def generateResponse(resp: ChannelBuffer) = {
    val x = new DefaultHttpResponse(HttpVersion.HTTP_1_1,
      HttpResponseStatus.OK)
    x.setContent(resp)
    x
  }
  def resolveRoute: PartialFunction[String, DefaultHttpResponse]
}
