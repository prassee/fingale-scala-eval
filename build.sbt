
name := "finagle-eval"

autoCompilerPlugins := true

scalaVersion := "2.10.0"

libraryDependencies += "com.twitter" %% "finagle-http" % "6.2.0"
