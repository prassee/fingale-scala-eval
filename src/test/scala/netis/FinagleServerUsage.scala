package netis

object FinagleServerUsage extends App {
  NetisServer.start(ServerBootStrap(9090, "connectorX-Server", new MockHanlder))
}

class MockHanlder extends RouteHandler {
  def resolveRoute = {
    case "/" => {
      generateResponse(contentResp("asdfasdf"))
    }
  }
}
