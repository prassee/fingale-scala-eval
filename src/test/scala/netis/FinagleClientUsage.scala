package netis

import com.twitter.finagle.builder.ClientBuilder
import org.jboss.netty.handler.codec.http.DefaultHttpRequest
import com.twitter.finagle.Service
import org.jboss.netty.handler.codec.http.HttpResponse
import org.jboss.netty.handler.codec.http.HttpRequest
import org.jboss.netty.handler.codec.http.HttpVersion
import org.jboss.netty.handler.codec.http.HttpMethod
import com.twitter.finagle.http.Http

object FinagleClientUsage extends App {
  
  
  // Don't worry, we discuss this magic "ClientBuilder" later
  val client: Service[HttpRequest, HttpResponse] = ClientBuilder()
    .codec(Http())
    .hosts("localhost:9090") // If >1 host, client does simple load-balancing
    .hostConnectionLimit(1)
    .build()

  val req = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/")

  val f = client(req) // Client, send the request

  // Handle the response:
  f onSuccess { res =>
    println("got response", new String(res.getContent().array()))
  } onFailure { exc =>
    println("failed :-(", exc)
  }

}